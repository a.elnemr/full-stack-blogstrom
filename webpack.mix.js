const mix = require('laravel-mix');

mix.react('resources/js/maelstrom.js', 'public/js');

mix.postCss('vendor/maelstrom-cms/toolkit/ui/css/maelstrom.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
]);

mix.webpackConfig({
    module: {
        rules: [require('@maelstrom-cms/toolkit/js/support/DontIgnoreMaelstrom')()],
    },
});

mix.react('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
