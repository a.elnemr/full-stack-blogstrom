<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Maelstrom\Panel;
use ReflectionException;

class PostController extends Controller
{
    /**
     * @var Panel
     */
    private $panel;

    public function __construct()
    {
        $this->panel = maelstrom(Post::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->panel->setTableHeadings([
            [
                'label' => 'Image',
                'type' => 'MediaManagerColumn',
                'name' => 'image',
            ],
            [
                'label' => 'Name',
                'name' => 'name',
                'searchable' => true,
                'sortable' => true,
                'type' => 'EditLinkColumn'
            ],
            [
                'label' => 'Slug',
                'name' => 'slug',
                'searchable' => true,
                'sortable' => true,
                'type' => 'EditLinkColumn'
            ]
        ]);

        return $this->panel->index('admin.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return $this->panel->create('admin.post.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ReflectionException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|max:191',
            'body' => 'required',
            'image' => 'required',
            'is_publish' => 'required',
            'category_id' => 'required',
        ]);

        $this->panel->store($request->get('name') . ' has been created!');

        return $this->panel->redirect('edit');
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return Response
     */
    public function show(Post $post)
    {
        return redirect()->route('post.edit', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return Response
     */
    public function edit(Post $post)
    {
        $this->panel->setEntry($post);

        return $this->panel->edit('admin.post.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Post $post
     * @return Response
     * @throws ReflectionException
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);

        $this->panel->setEntry($post);

        $this->panel->update($post->name . ' has been updated');

        return $this->panel->redirect('edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return void
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $this->panel->setEntry($post);

        $message = $post->exists() ? ($post->name . ' has been deleted.') : ($post->name . ' has been restored.');

        $this->panel->destroy($message);

        return $this->panel->redirect(
            $post->exists() ? 'edit' : 'index'
        );
    }

    /**
     * Handles the bulk actions from the index table
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function bulk()
    {
        $this->panel->handleBulkActions();

        return $this->panel->redirect('index');
    }
}
