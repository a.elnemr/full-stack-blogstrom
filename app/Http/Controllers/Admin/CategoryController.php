<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maelstrom\Panel;
use ReflectionException;

class CategoryController extends Controller
{
    /**
     * @var Panel
     */
    private $panel;

    public function __construct()
    {
        $this->panel = maelstrom(Category::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->panel->setTableHeadings([
            [
                'label' => 'Name',
                'name' => 'name',
                'searchable' => true,
                'sortable' => true,
                'type' => 'EditLinkColumn'
            ],
            [
                'label' => 'DeletedAt',
                'name' => 'deleted_at',
                'searchable' => true,
                'sortable' => true,
                'type' => 'EditLinkColumn'
            ]
        ]);

        return $this->panel->index('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return $this->panel->create('admin.category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ReflectionException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $this->panel->store($request->get('name') . ' has been created!');

        return $this->panel->redirect('edit');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function show(Category $category)
    {
        return redirect()->route('category.edit', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        $this->panel->setEntry($category);

        return $this->panel->edit('admin.category.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return Response
     * @throws ReflectionException
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);

        $this->panel->setEntry($category);

        $this->panel->update($category->name . ' has been updated');

        return $this->panel->redirect('edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return void
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $this->panel->setEntry($category);

        $message = $category->exists() ? ($category->name . ' has been deleted.') : ($category->name . ' has been restored.');

        $this->panel->destroy($message);

        return $this->panel->redirect(
            $category->exists() ? 'edit' : 'index'
        );
    }

    /**
     * Handles the bulk actions from the index table
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function bulk()
    {
        $this->panel->handleBulkActions();

        return $this->panel->redirect('index');
    }
}
