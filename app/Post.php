<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Maelstrom\Models\Media;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'slug',
        'image',
        'body',
        'is_publish',
        'category_id',
        'tags'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }


    public function featuredImage()
    {
        return $this->belongsTo(Media::class, 'image');
    }
}
