<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('maelstrom_sidebar', [
            [
                'id' => 'content',
                'label' => 'Content',
                'type' => 'SubMenu',
                'icon' => 'edit',
                'children' => [
                    [
                        'id' => 'categories',
                        'label' => 'Categories',
                        'url' => url('/admin/category'),
                        'icon' => 'folder-open',
                    ],
                    [
                        'id' => 'posts',
                        'label' => 'Posts',
                        'url' => url('/admin/post'),
                        'icon' => 'read',
                    ]
                ],
            ],
        ]);
    }
}
