<?php


Route::prefix('/admin')
    ->namespace('Admin')
    ->group(function () {
    Route::resource('category', 'CategoryController');
    Route::post('category/bulk', 'CategoryController@bulk')->name('category.bulk');

    Route::resource('post', 'PostController');
    Route::post('post/bulk', 'PostController@bulk')->name('post.bulk');
});
