@extends('maelstrom::layouts.form')

@section('content')

    @component('maelstrom::components.form', [
        'action' => $action,
        'method' => $method,
    ])

        @include('maelstrom::inputs.text', [
            'label' => 'Category Name',
            'name' => 'name',
            'required' => true,
        ])

    @endcomponent

@endsection
