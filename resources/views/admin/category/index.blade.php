@extends('maelstrom::layouts.index')

@section('buttons')
    @include('maelstrom::buttons.button', [
        'url' => route('category.create'),
        'label' => 'Create Category'
    ])
@endsection
