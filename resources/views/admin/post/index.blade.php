@extends('maelstrom::layouts.index')

@section('buttons')
    @include('maelstrom::buttons.button', [
        'url' => route('post.create'),
        'label' => 'Create Post'
    ])
@endsection
