@extends('maelstrom::layouts.form')

@section('content')

    @component('maelstrom::components.form', [
        'action' => $action,
        'method' => $method,
    ])

        @include('maelstrom::inputs.text', [
            'label' => 'Post Name',
            'name' => 'name',
            'required' => true,
        ])

        @include('maelstrom::inputs.text', [
            'label' => 'URL Slug',
            'name' => 'slug',
            'required' => true,
        ])

        @include('maelstrom::components.media_manager', [
            'label' => 'Featured Image',
            'name' => 'image',
            'required' => true,
        ])

        @include('maelstrom::inputs.switch', [
            'label' => 'Is Published?',
            'name' => 'is_publish',
        ])

        @include('maelstrom::inputs.wysiwyg', [
           'label' => 'Body Content',
           'name' => 'body',
           'required' => true,
       ])

        <div class="flex flex-wrap">
            <div class="w-1/2 pr-10">

                @include('maelstrom::inputs.select', [
                    'label' => 'Category',
                    'name' => 'category_id',
                    'options' => [],
                    'required' => true,
                    'remote_uri' => route('maelstrom.form-options', 'categories'),
                    'create_button' => [
                        'url' => route('category.create'),
                    ],
                ])

            </div>
            <div class="w-1/2">

                @include('maelstrom::inputs.tags', [
                    'label' => 'Tags',
                    'name' => 'tags',
                ])

            </div>
        </div>

    @endcomponent

@endsection
